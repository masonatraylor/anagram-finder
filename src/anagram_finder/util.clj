(ns anagram-finder.util
  (:require [compojure.core :refer :all]
            [clojure.string :as str]))

(defn key [word] (sort (str/lower-case word)))

(defn limit-fn [limit words] (if (nil? limit) words (take (read-string limit) words)))

(defn is-proper [word] (re-find #"^[A-Z]" word))

(defn remove-proper-nouns-fn [include-proper-nouns words] (if (= include-proper-nouns "true") words (remove is-proper words)))

(defn count-comparator [a b] (compare (count a) (count b)))