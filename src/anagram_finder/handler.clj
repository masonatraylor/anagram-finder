(ns anagram-finder.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
            [ring.middleware.json :refer [wrap-json-params]]
            [ring.middleware.json :refer [wrap-json-response]]
            [clojure.string :as str]
            [anagram-finder.util :refer :all]))

(def anagram-map {})

(defn find-anagrams
  ([word] (remove #{word} (get anagram-map (key word))))
  ([word limit include-proper-nouns] (let [words (limit-fn limit
                                                           (remove-proper-nouns-fn include-proper-nouns
                                                                                   (find-anagrams word)))]
                                       {:body {:anagrams words}})))

(defn all-words-sorted [] (sort count-comparator (flatten (vals anagram-map))))

(defn compute-stats [] (let [all-words (all-words-sorted)
                             word-count (count all-words)]
                         {:body {:count word-count
                                 :min (count (first all-words))
                                 :max (count (last all-words))
                                 :average (if (= 0 word-count) 0 (/ (reduce + (map count all-words)) word-count))
                                 :median (if (= 0 word-count) 0 (count (nth all-words (/ word-count 2))))}}))

(defn anagram-list [minimum] (let [anagrams (concat (filter #(>= (count %) minimum) (vals anagram-map)))]
                               {:body {:anagrams anagrams}}))

(defn max-anagrams [] (let [max-count (apply max (cons 0 (map count (vals anagram-map))))]
                         (anagram-list max-count)))

(defn add-word [word] (def anagram-map (assoc anagram-map (key word) (cons word (find-anagrams word)))))

(defn add-words [words] (doseq [word words] (add-word word))
  {:status 201})

(defn read-dictionary [] (add-words (str/split (slurp "resources/dictionary.txt") #"\n"))
  {:status 201})

(defn are-anagrams [words] (let [target-key (key (first words)) all-equivalent (every? #(= target-key %) (map key words))]
                             {:body {:are-anagrams all-equivalent}}))

(defn delete-anagrams [word] (def anagram-map (dissoc anagram-map (key word)))
  {:status 204})

(defn delete-word [word] (def anagram-map (assoc anagram-map (key word) (find-anagrams word)))
  {:status 204})

(defn delete-words [] (def anagram-map {})
  {:status 204})

(defroutes app-routes
  (GET "/anagrams/:word.json" [word limit include-proper-nouns] (find-anagrams word limit include-proper-nouns))
  (GET "/words/stats.json" [] (compute-stats))
  (GET "/words/anagram-list.json" [minimum] (anagram-list (read-string (or minimum "0"))))
  (GET "/words/max.json" [] (max-anagrams))
  (POST "/words.json" [words] (add-words words))
  (POST "/dictionary" [] (read-dictionary))
  (POST "/words/are-anagrams.json" [words] (are-anagrams words))
  (DELETE "/words/:word.json" [word include-anagrams] (if (= include-anagrams "true") (delete-anagrams word) (delete-word word)))
  (DELETE "/words.json" [] (delete-words))
  (route/not-found "Not Found"))

(def app (-> app-routes
             (wrap-json-params)
             (wrap-json-response)
             (wrap-defaults api-defaults)))
