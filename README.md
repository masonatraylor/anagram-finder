# anagram-finder

This is a simple anagram-finding webapp that leverages Clojure, Ring and Compojure. The endpoints are as follows:

`GET /anagrams/:word.json` - Returns an array of all anagrams found in the dictionary for the given word.
Accepts an optional `limit` query parameter that will limit the number of anagram results.
Also accepts an optional `include-proper-nouns` query parameter that will include proper nouns in the anagram list.

`GET /words/stats.json` - Returns the count of words in the dictionary and the average, minimum, maximum, and median word lengths.

`GET /words/anagram-list.json` - Returns all sets of words in the dictionary that are anagramatically-equivalent.
Accepts an optional `minimum` query parameter that will restrict anagram sets to have at least a `minimum` size.

`GET /words/max.json` - Returns the set of words with the most anagrams in the dictionary.

`POST /words.json` - Adds all strings in the `words` property of the request body to the anagram dictionary.

`POST /dictionary` - Populates the anagram dictionary with a predefined set of ~235000 words.

`POST /words/are-anagrams.json` - Returns a flag that specifies if all the strings in the `words` property of the request body are anagrams. Is not case-sensitive.

`DELETE /words/:word.json` - Removes the specified word from the dictionary. Also removes all of the word's anagrams when the optional `include-anagrams` query parameter is set to `true`

`DELETE /words.json` - Deletes all words from the dictionary.


## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein ring server

## License

Copyright © 2019 Mason Traylor

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
